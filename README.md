# Install tileserver-gl for self hosting Map Services

1. Create a project folder.

> mkdir %USERPROFILE%\mapserver

2. Download `data` folder to the project root: `%USERPROFILE%\mapserver\data`

3. Download **[free map data](https://openmaptiles.com/downloads/planet/)** and put it to `data\mbtiles` folder.

4. Create a docker volume to host `%USERPROFILE%\mapserver\data` in container.

> docker volume rm map-data

> docker volume create map-data

> docker run -itd --rm --name volume_host -v map-data:/tmp busybox

> docker cp %USERPROFILE%\mapserver\data volume_host:\tmp

6. Install **[tileserver-gl](https://tileserver.readthedocs.io/en/latest/)** docker version:

> docker run --rm -it -v map-data:/data -p 8000:80 maptiler/tileserver-gl --verbose

7. Point your browser to `http://localhost:8000`. And you should be able to view the map.

### References: 
1. Customize map styles: https://openmaptiles.org/docs/style/maputnik/
2. Consume Map Services using Mapbox GL js: https://openmaptiles.org/docs/website/mapbox-gl-js/
3. Get inspired: https://cloud.maptiler.com/

### Screenshots
**Home**

![Home](https://gitlab.com/jeff.j2ee/data-speaks-louder/-/raw/master/home.jpg)

**basic**

![basic](https://gitlab.com/jeff.j2ee/data-speaks-louder/-/raw/master/basic.jpg)

**hybrid**

![hybrid](https://gitlab.com/jeff.j2ee/data-speaks-louder/-/raw/master/hybrid.jpg)

**osm-liberty**

![osm-liberty](https://gitlab.com/jeff.j2ee/data-speaks-louder/-/raw/master/osm-liberty.jpg)

**dark-matter**

![dark-matter](https://gitlab.com/jeff.j2ee/data-speaks-louder/-/raw/master/dark-matter.jpg)
