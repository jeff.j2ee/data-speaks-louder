## Example: A React-map-GL app to consume tileserver-GL Map Services
Demonstrates how to create an interactive react-map-gl app with self hosted tileserver-GL Map Services (@**port: 8000**).

To run: `npm i && npm start`

Run in docker: `docker-compose up` (Note: please have the volume `map-data` up and running)

![screenshot](https://gitlab.com/jeff.j2ee/data-speaks-louder/-/raw/master/app/app.jpg)

![screenshot](https://gitlab.com/jeff.j2ee/data-speaks-louder/-/raw/master/app/app-dark.jpg)

![screenshot](https://gitlab.com/jeff.j2ee/data-speaks-louder/-/raw/master/app/app-light.jpg)
