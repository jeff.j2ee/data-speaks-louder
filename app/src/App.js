import React, { useState, useEffect, useReducer, useCallback } from 'react';
import ReactMapGL, { Marker, Popup, Source, Layer, FlyToInterpolator, NavigationControl } from 'react-map-gl';
import {flights, geoRoutes, animateRoutes, svg_icon, svg_plane, themes} from './plotdata';

const center = {
    orig: {
        latitude: 19.741755,
        longitude: -155.844437,
        width: '100vw',
        height: '100vh',
        zoom: 10
    },
    dest: {
        latitude: 37.9870,
        longitude: -97.4119,
        width: '100vw',
        height: '100vh',
        zoom: 4.3,
        transitionInterpolater: new FlyToInterpolator(),
        transitionDuration: 4000
    }
};

export default function App() {
    const [state, dispatch] = useReducer((prevState, newState) => ({...prevState, ...newState}), {});
    const [theme, setTheme] = useState('dark-matter');
    const [selected, setSelected] = useState();
    const [viewport, setViewport] = useState(center.orig);

    const trace = useCallback(({point, lngLat}) => dispatch({point, lngLat}), []);
    const onViewportChange = useCallback((viewport) => setViewport(viewport), []);
    const handleThemeChange = useCallback((theme) => () => setTheme(theme), []);
    const toggleAnimation = useCallback(() => {
        if (animateRoutes.rafid == null) {
            window.requestAnimationFrame(() => animateRoutes(dispatch));
        } else {
            window.cancelAnimationFrame(animateRoutes.rafid);
            delete animateRoutes.rafid;
        }
    }, []);

    useEffect(() => {
        import('./cities.json').then(({ default: data }) => {
            const routes = flights.map((route) => route.map(name => {
                const city = data.find(d => d.city === name);
                return [city.longitude, city.latitude].map(d => parseFloat(d));
            }));

            dispatch({data, routes: geoRoutes(routes)});
        });

        const keydown = (e) => e.key === 'Escape' && setSelected();
        const resize = () => onViewportChange(center.dest);

        resize();

        window.addEventListener('keydown', keydown);
        window.addEventListener('resize', resize);

        return () => {
            window.removeEventListener('keydown', keydown);
            window.removeEventListener('resize', resize);
            window.cancelAnimationFrame(animateRoutes.rafid);
        };
    }, [onViewportChange]);

    return (
        <ReactMapGL {...viewport}
            maxZoom={20}
            onMouseUp={trace}
            onViewportChange={onViewportChange}
            mapStyle={`http://localhost:8000/styles/${theme}/style.json`}
        >
            <RouteSource routes={state.routes} />
            <PlaneMarker points={state.points} />
            <MapMarker markers={state.data} select={setSelected} />
            <InfoPopup location={selected} reset={setSelected} />
            <ControlPanel active={theme} handleThemeChange={handleThemeChange} toggleAnimation={toggleAnimation} />
            <div className="top-right">
                <NavigationControl />
            </div>
            <div className="bottom-left">
                <div className="trace-label">
                    <div><span>x, y</span>: [{ (state.point || []).map(d => d.toFixed(4)).join(', ')}]</div>
                    <div><span>Lng, Lat</span>: [{ (state.lngLat || []).map(d => d.toFixed(4)).join(', ')}]</div>
                </div>
            </div>
        </ReactMapGL>
    );
};

function RouteSource({routes}) {
    if (!routes) return null;
    return (
        <Source id="routes" key="routes" type="geojson" data={routes}>
            <Layer {...{
                type: 'line',
                paint: {'line-width': 0.6, 'line-color': '#007cbf'}
            }} />
        </Source>
    );
}

function MapMarker({markers, select}) {
    if (!markers) return null;
    return markers.map((city, index) => (
        <Marker key={`marker-${index}`} latitude={city.latitude} longitude={city.longitude}>
            <svg className="svg-marker" onClick={(e) => select(city)}>
                <path d={svg_icon} />
            </svg>
        </Marker>
    ));
}

function InfoPopup({location, reset}) {
    if (!location) return null;
    const { longitude, latitude, city, state, image, population } = location;
    const displayname = `${city}, ${state}`;
    return (
        <Popup anchor="top" latitude={latitude} longitude={longitude} closeOnClick={true} onClose={() => reset()}>
            <div>
                <div className="content-header">
                    <span>{displayname}</span>
                    <span>Population: {population}</span>
                </div>
                <img width={350} src={image} alt={displayname} />
            </div>
        </Popup>
    );
}

function PlaneMarker({points}) {
    if (!points) return null;
    return points.features.map(({geometry: {coordinates: [longitude, latitude]}, properties: {bearing}}, index) => {
        const props = {
            className: 'svg-plane',
            style: {transform: `translate(-13px, -13px) rotate(${bearing}deg)`}
        };
        return (
            <Marker key={`plane-${index}`} latitude={latitude} longitude={longitude}>
                <svg {...props}><path d={svg_plane} /></svg>
            </Marker>
        );
    });
}

function ControlPanel({active, handleThemeChange, toggleAnimation}) {
    const themeControl = themes.map(([key, label]) => {
        const props = {key, type: 'button', title: label, onClick: handleThemeChange(key)};
        if (key === active) props.className = 'active';
        return (
            <button {...props}>
                <span aria-hidden="true">{label}</span>
            </button>
        );
    });
    themeControl.push((
        <button key="pause" type="button" onClick={toggleAnimation}>
            <span aria-hidden="true">Toggle Animation</span>
        </button>
    ));
    return (
        <div className="top-left">
            <div className="nav">{themeControl}</div>
        </div>
    );
}
