import * as turf from '@turf/turf';

export const flights = [
    ['New York', 'Indianapolis'],
    ['Los Angeles', 'Chicago'],
    ['Los Angeles', 'New York'],
    ['Los Angeles', 'Jacksonville'],
    ['Los Angeles', 'Denver'],
    ['Denver', 'Philadelphia'],
    ['Denver', 'Jacksonville'],
    ['Denver', 'San Antonio'],
    ['Chicago', 'Seattle'],
    ['Seattle', 'Charlotte'],
    ['Seattle', 'San Diego'],
    ['Dallas', 'Charlotte'],
    ['Indianapolis', 'Dallas'],
    ['Denver', 'Seattle'],
    ['San Jose', 'Houston'],
    ['New York', 'Chicago'],
    ['El Paso', 'Chicago'],
    ['New York', 'San Francisco'],
    ['New York', 'Jacksonville'],
    ['San Diego', 'Jacksonville']
];

export const themes = [
    ['basic', 'Light'],
    ['hybrid', 'Satellite'],
    ['osm-liberty', 'Liberty'],
    ['dark-matter', 'Dark']
];

export const svg_icon = `M182.9,551.7c0,0.1,0.2,0.3,0.2,0.3S358.3,283,358.3,194.6c0-130.1-88.8-186.7-175.4-186.9
   C96.3,7.9,7.5,64.5,7.5,194.6c0,88.4,175.3,357.4,175.3,357.4S182.9,551.7,182.9,551.7z M122.2,187.2c0-33.6,27.2-60.8,60.8-60.8
   c33.6,0,60.8,27.2,60.8,60.8S216.5,248,182.9,248C149.4,248,122.2,220.8,122.2,187.2z`;

export const svg_plane = `m25.21488,3.93375c-0.44355,0 -0.84275,0.18332 -1.17933,0.51592c-0.33397,0.33267 -0.61055,
    0.80884 -0.84275,1.40377c-0.45922,1.18911 -0.74362,2.85964 -0.89755,4.86085c-0.15655,1.99729 -0.18263,
    4.32223 -0.11741,6.81118c-5.51835,2.26427 -16.7116,6.93857 -17.60916,7.98223c-1.19759,1.38937 -0.81143,
    2.98095 -0.32874,4.03902l18.39971,-3.74549c0.38616,4.88048 0.94192,9.7138 1.42461,13.50099c-1.80032,
    0.52703 -5.1609,1.56679 -5.85232,2.21255c-0.95496,0.88711 -0.95496,3.75718 -0.95496,3.75718l7.53,
    -0.61316c0.17743,1.23545 0.28701,1.95767 0.28701,1.95767l0.01304,0.06557l0.06002,0l0.13829,0l0.0574,
    0l0.01043,-0.06557c0,0 0.11218,-0.72222 0.28961,-1.95767l7.53164,0.61316c0,0 0,-2.87006 -0.95496,
    -3.75718c-0.69044,-0.64577 -4.05363,-1.68813 -5.85133,-2.21516c0.48009,-3.77545 1.03061,-8.58921 1.42198,
    -13.45404l18.18207,3.70115c0.48009,-1.05806 0.86881,-2.64965 -0.32617,-4.03902c-0.88969,-1.03062 -11.81147,
    -5.60054 -17.39409,-7.89352c0.06524,-2.52287 0.04175,-4.88024 -0.1148,-6.89989l0,-0.00476c-0.15655,
    -1.99844 -0.44094,-3.6683 -0.90277,-4.8561c-0.22699,-0.59493 -0.50356,-1.07111 -0.83754,-1.40377c-0.33658,
    -0.3326 -0.73578,-0.51592 -1.18194,-0.51592l0,0l-0.00001,0l0,0l0.00002,0.00001z`;

const datasets = {};

export function geoRoutes(routes) {
    // A multi lines from origins to destinations.
    const lines = routes.map(line => {
        const geoLine = turf.lineString(line);

        // Number of steps to use in the arc and animation, more steps means
        // a smoother arc and animation, but too many steps will result in a
        // low frame rate
        const frames = Math.floor(400 + Math.random() * 400);

        // Calculate the distance in kilometers between route start/end point.
        const lineDistance = turf.lineDistance(geoLine);
        const step = lineDistance / frames;

        // Draw an arc between the `origin` & `destination` of the two points
        return [...Array(frames).keys()].map(i => {
            const segment = turf.along(geoLine, i * step);
            return segment.geometry.coordinates;
        });
    });

    datasets.paths = lines.map(line => turf.lineString(line, {counter: 0}));

    return turf.featureCollection(datasets.paths);
};

export function geoPoints() {
    // update coordinate with new one denoted by feature property counter.
    const points = datasets.paths.map(({geometry: {coordinates: path}, properties}) => {
        const frame = properties.counter;

        // Calculate the bearing to ensure the icon is rotated to match the route arc
        // The bearing is calculate between the current point and the next point, except
        // at the end of the arc use the previous point and the current point
        const params = path.slice(frame, frame + 2).map(d => turf.point(d));
        if (params.length < 2) params.unshift(turf.point(path[frame - 1]));

        properties.counter++;

        if (properties.counter >= path.length) properties.counter = 0;

        return turf.point(path[frame], {bearing: turf.bearing(...params)});
    });

    return turf.featureCollection(points);
};

export function animateRoutes(dispatch) {
    dispatch({points: geoPoints()});
    animateRoutes.rafid = window.requestAnimationFrame(() => animateRoutes(dispatch));
};
